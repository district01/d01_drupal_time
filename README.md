# D01 Drupal Time

DO1 class for handling Time. 

This class resets the date part of the timestamp
to today so any calculation that are made will use the same day (today). 
This means that by comparing timestamps, ... only the time part will be 
compared and that is what we want since this class in used solely for time calculations.

# Release notes #

`1.4`
* Fixed Drupal 9 compatibility.

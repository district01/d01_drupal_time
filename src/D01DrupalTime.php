<?php

namespace Drupal\d01_drupal_time;

use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Class Time.
 *
 * This class represents a time object.
 * Times are used as timestamps.
 *
 * This class resets the date part of the timestamp
 * to today so any calculation that are made will use the
 * same day (today). This means that by comparing timestamps, ...
 * only the time part will be compared and that is what we want
 * since this class in used solely for time calculations.
 *
 * @package Drupal\d01_drupal_time
 */
class D01DrupalTime extends DrupalDateTime {

  /**
   * Drupal DateTime object.
   *
   * @var \Drupal\Core\Datetime\DrupalDateTime
   */
  protected $dateTimeObject = NULL;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime|null $datetime
   *   A drupal date time object used solely for time parts.
   */
  public function __construct(DrupalDateTime $datetime = NULL) {
    $this->dateTimeObject = $this->prepareD01DrupalTime($datetime);
  }

  /**
   * Prepare datetime object.
   *
   * We reset the date part to today so any calculation
   * that are made will use the same day (today).
   * This means that by comparing timestamps, ...
   * only the time part will be compared.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $datetime
   *   A drupal date time object used solely for time parts.
   */
  private function prepareD01DrupalTime(DrupalDateTime $datetime = NULL) {
    $prepared_datetime = new DrupalDateTime('now');
    if ($datetime) {
      // Do NOT set a timzeon, as this will correct the hours offet. We want the
      // original time as entered in the timezone offset at the time of
      // entering.
      $prepared_datetime->setTime($datetime->format('H'), $datetime->format('i'), $datetime->format('s'));
    }
    return $prepared_datetime;
  }

  /**
   * Implements the magic __call method.
   *
   * Passes through all unknown calls onto the DateTime object.
   *
   * @param string $method
   *   The method to call on the decorated object.
   * @param array $args
   *   Call arguments.
   *
   * @return mixed
   *   The return value from the method on the decorated object. If the proxied
   *   method call returns a DateTime object, then return the original
   *   DateTimePlus object, which allows function chaining to work properly.
   *   Otherwise, the value from the proxied method call is returned.
   *
   * @throws \Exception
   *   Thrown when the DateTime object is not set.
   * @throws \BadMethodCallException
   *   Thrown when there is no corresponding method on the DateTime object to
   *   call.
   */
  public function __call($method, array $args) {
    // @todo consider using assert() as per https://www.drupal.org/node/2451793.
    if (!isset($this->dateTimeObject)) {
      throw new \Exception('DateTime object not set.');
    }

    // The DrupalDateTime class will handle exceptions for methods
    // that don't exist on the date time object so we can safely
    // call the method without any checks in this class.
    $result = call_user_func_array([$this->dateTimeObject, $method], $args);
    return $result === $this->dateTimeObject ? $this : $result;
  }

  /**
   * Implements the magic __callStatic method.
   *
   * Passes through all unknown static calls onto the DateTime object.
   */
  public static function __callStatic($method, $args) {
    if (!method_exists('\DateTime', $method)) {
      throw new \BadMethodCallException(sprintf('Call to undefined method %s::%s()', get_called_class(), $method));
    }
    return call_user_func_array(['\DateTime', $method], $args);
  }

  /**
   * Implements the magic __clone method.
   *
   * Deep-clones the DateTime object we're wrapping.
   */
  public function __clone() {
    $this->dateTimeObject = clone($this->dateTimeObject);
  }

}
